const express = require("express");
const app = express();
const request = require("request");

const fs = require('fs');


process.env.NODE_ENV || (process.env.NODE_ENV = 'development');
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

app.use(express.json());

function fileFinder(req) {
    const router = JSON.parse(fs.readFileSync('router.json'));
    const domain = router[(req.headers.host).replace('www.', '')];

    if (typeof domain[(req.params[0].replace('/', ''))] === 'undefined') return domain['default'];
    return domain[(req.params[0].replace('/', ''))];
}

app.get('*', (req, res) => {
    console.log("Domain", req.headers.host);
    console.log("Path", req.params[0]);
    req.pipe(request("https://storage.googleapis.com/mindmint-facilitator-websites/index.html")).pipe(res);
    //req.pipe(request(fileFinder(req))).pipe(res);
})

const port = process.env.PORT || 3001;

app.listen(port, "0.0.0.0", () => console.log(`Running on port ${port}`));



// const app = require('express')();
// const express = require('express');
// const https = require('https');
// const fs = require('fs');

// const request = require("request");

// process.env.NODE_ENV || (process.env.NODE_ENV = 'development');
// if (process.env.NODE_ENV !== 'production') {
//     require('dotenv').config();
// }

// app.use(express.json());

// function fileFinder(req) {
//     const router = JSON.parse(fs.readFileSync('router.json'));
//     const domain = router[(req.headers.host).replace('www.', '')];

//     if (typeof domain[(req.params[0].replace('/', ''))] === 'undefined') return domain['default'];
//     return domain[(req.params[0].replace('/', ''))];
// }

// app.get('*', (req, res) => {
//     console.log("Domain", req.headers.host);
//     console.log("Path", req.params[0]);
//     req.pipe(request(fileFinder(req))).pipe(res);
// })

// const port = process.env.PORT || 3001;

// // we will pass our 'app' to 'https' server
// https.createServer({
//     key: fs.readFileSync('../key.pem'),
//     cert: fs.readFileSync('../cert.pem'),
//     passphrase: 'hello123!'
// }, app).listen(port, "0.0.0.0", () => console.log(`Running on port ${port}`));